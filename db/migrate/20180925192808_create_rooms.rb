class CreateRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :rooms do |t|
      t.integer :num_bed
      t.string :room_number
      t.references :Hotel, foreign_key: true

      t.timestamps
    end
  end
end
