class Hotel < ApplicationRecord
  has_many :rooms, inverse_of: :hotels
  accepts_nested_attributes_for :rooms, reject_if: :all_blank, allow_destroy: true
end
