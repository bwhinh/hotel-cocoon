class Room < ApplicationRecord
  belongs_to :Hotel, inverse_of: :rooms
end
